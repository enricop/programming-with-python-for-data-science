import pandas as pd
from sklearn.cross_validation import train_test_split
from sklearn import tree
import numpy as np

#https://archive.ics.uci.edu/ml/machine-learning-databases/mushroom/agaricus-lepiota.names


# 
# Load up the mushroom dataset into dataframe 'X'
# Verify you did it properly.
# Indices shouldn't be doubled.
# Header information is on the dataset's website at the UCI ML Repo
# Check NA Encoding
#
col_names = ['class', 'cap-shape', 'cap-surface', 'cap-color', 'bruises?', 'odor',
             'gill-attachment', 'gill-spacing', 'gill-size', 'gill-color', 'stalk-shape',
             'stalk-root', 'stalk-surface-above-ring', 'stalk-surface-below-ring' ,
             'stalk-color-above-ring', 'stalk-color-below-ring', 'veil-type', 'veil-color',
             'ring-number', 'ring-type', 'spore-print-color', 'population', 'habitat']
X = pd.read_csv("Datasets/agaricus-lepiota.data", header=None, names = col_names)

# INFO: An easy way to show which rows have nans in them
#print X[pd.isnull(X).any(axis=1)]

# 
# Go ahead and drop any row with a nan
#
# .. your code here ..
print X.shape


#
# Copy the labels out of the dset into variable 'y' then Remove
# them from X. Encode the labels, using the .map() trick we showed
# you in Module 5 -- canadian:0, kama:1, and rosa:2
#
y = X.loc[:, "class"]
X.drop(labels = ['class'], axis=1, inplace=1)
y = y.astype("category").cat.codes

#
# Encode the entire dataset using dummies
#
X = pd.get_dummies(X)

# 
# Split your data into test / train sets
# Your test size can be 30% with random_state 7
# Use variable names: X_train, X_test, y_train, y_test
#
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30, random_state=1)

#
# Create an DT classifier. No need to specify any parameters
#
dt = tree.DecisionTreeClassifier()
 
#
# train the classifier on the training data / labels:
dt.fit(X_train, y_train)
# score the classifier on the testing data / labels:
#
score = dt.score(X_test, y_test)
print ("High-Dimensionality Score: ", round((score*100), 3))
print "High-Dimensionality Score: ", round((score*100), 3)

importances = dt.feature_importances_
indices = np.argsort(importances)[::-1]

# Print the feature ranking
print("Feature ranking:")

for f in range(X.shape[1]):
    print("%d. feature %d (%f) - %s" % (f + 1, indices[f], importances[indices[f]], X.columns.values[f]))
#
# Use the code on the courses SciKit-Learn page to output a .DOT file
# Then render the .DOT to .PNGs. Ensure you have graphviz installed.
# If not, `brew install graphviz`.
#
tree.export_graphviz(dt, out_file = 'tree.dot')