import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
#
# Load up the Seeds Dataset into a Dataframe
# It's located at 'Datasets/wheat.data'
# 
# .. your code here ..
df = pd.read_csv("Datasets/wheat.data", index_col=0, header=0)

#
# Drop the 'id' feature
# 
#df = df.drop('id', axis=1)

#
# Compute the correlation matrix of your dataframe
# 
df.corr()

#
# Graph the correlation matrix using imshow or matshow
# 
plt.imshow(df.corr(), cmap=plt.cm.Blues, interpolation='nearest')
plt.colorbar()
tick_marks = [i for i in range(len(df.columns))]
plt.xticks(tick_marks, df.columns, rotation='vertical')
plt.yticks(tick_marks, df.columns)

plt.show()