import pandas as pd
from sklearn.svm import SVC
from sklearn.cross_validation import train_test_split

X = pd.read_csv('Datasets/parkinsons.data')

y = X[['status']]

X.drop(labels=['name','status'], axis=1, inplace=1)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30, random_state=7)

svc = SVC()
svc.fit(X_train, y_train)
score = svc.score(X_test, y_test)

print ("Accuracy of the default SVC model : ", score)        