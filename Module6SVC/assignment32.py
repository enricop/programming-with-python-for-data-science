import pandas as pd
import numpy as np
from sklearn.svm import SVC
from sklearn.cross_validation import train_test_split

X = pd.read_csv('Datasets/parkinsons.data')

y = X[['status']]

X.drop(labels=['name','status'], axis=1, inplace=1)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30, random_state=7)

best_score = 0
C = 0
gamma = 0

for i in np.arange(0.05, 2, 0.05):
    for j in np.arange(0.001, 0.1, 0.001):
        svc = SVC(kernel = 'rbf', C = i, gamma = j)
        svc.fit(X_train, y_train)
        score = svc.score(X_test, y_test)
        if score > best_score:
            best_score = score
            C = i
            gamma = j

print ("Best Accuracy : ", best_score) 
print ("C value for best accuracy: ", C)
print ("gamma value for best accuracy: ", gamma)



        