import pandas as pd
import numpy as np
from sklearn.svm import SVC
from sklearn.cross_validation import train_test_split
from sklearn import preprocessing

X = pd.read_csv('Datasets/parkinsons.data')

#Splice out the status column into a variable y and delete it from X.
y = X[['status']]

X.drop(labels=['name','status'], axis=1, inplace=1)

T = preprocessing.StandardScaler().fit_transform(X)
#T = preprocessing.MinMaxScaler().fit_transform(X)
#T = preprocessing.MaxAbsScaler().fit_transform(X)
#T = preprocessing.normalize(X)

X_train, X_test, y_train, y_test = train_test_split(T, y, test_size=0.30, random_state=7)

bscore = 0

for i in np.arange(0.05, 2, 0.05):
    for j in np.arange(0.001, 0.1, 0.001):
        svc = SVC(kernel = 'rbf', C = i, gamma = j)
        svc.fit(X_train, y_train)
        score = svc.score(X_test, y_test)
        if score > bscore:
            bscore = score

print ("Accuracy of the default SVC model : ", bscore)        