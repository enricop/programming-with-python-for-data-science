import pandas as pd
import numpy as np
from sklearn.svm import SVC
from sklearn.cross_validation import train_test_split
from sklearn import preprocessing
from sklearn.decomposition import PCA
from sklearn import manifold

X = pd.read_csv('Datasets/parkinsons.data')

y = X[['status']]

X.drop(labels=['name','status'], axis=1, inplace=1)

T = preprocessing.StandardScaler().fit_transform(X)
#T = preprocessing.MinMaxScaler().fit_transform(X)
#T = preprocessing.MaxAbsScaler().fit_transform(X)
#T = preprocessing.Normalize(X)

best_score = 0

for k in range(4, 14, 1):
    pca = PCA(n_components = k)
    pca.fit(T)
    pca_T = pca.transform(T)
    X_train, X_test, y_train, y_test = train_test_split(pca_T, y, test_size=0.30, random_state=7)
    
    for i in np.arange(0.05, 2, 0.05):
        for j in np.arange(0.001, 0.1, 0.001):
            svc = SVC(kernel = 'rbf', C = i, gamma = j)
            svc.fit(X_train, y_train)
            score = svc.score(X_test, y_test)
            if score > best_score:
                best_score = score

print ("Best Accuracy from PCA: ", best_score)

iso = manifold.Isomap(n_neighbors=2, n_components=4)
iso.fit(T)
iso_T = iso.transform(T)
X_train, X_test, y_train, y_test = train_test_split(iso_T, y, test_size=0.30, random_state=7)

for i in np.arange(0.05, 2, 0.05):
    for j in np.arange(0.001, 0.1, 0.001):
        svc = SVC(kernel = 'rbf', C = i, gamma = j)
        svc.fit(X_train, y_train)
        score = svc.score(X_test, y_test)
        if score > best_score:
            best_score = score

print ("Best Accuracy from ISO: ", best_score)